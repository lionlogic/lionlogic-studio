<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

    public $language;

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('Spyc');
        $this->load->helper('url');

        if (isset($_SESSION['language'])) {
            $this->language['key'] = $_SESSION['language'];
        } else {
            $this->language['key'] = 'ru';
        }

        $this->language['translate'] = Spyc::YAMLLoad($_SERVER['DOCUMENT_ROOT'].'/assets/translations/'.$this->language['key'].'.yaml');
    }

    public function index()
    {
        $data['language'] = $this->language;

        $this->load->view('include/header', $data);
        $this->load->view('include/nav', $data);
        $this->load->view('index', $data);
        $this->load->view('include/footer', $data);
    }

    public function changeLoc()
    {
        $_SESSION['language'] = $this->uri->segment(3);
        redirect('/');
    }
}