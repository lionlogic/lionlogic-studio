<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<header class="header">
    <aside class="left-side">
        <ul>
            <li><a href="https://vk.com/lion_logic" target="_blank">VK</a></li>
            <li><a href="https://www.instagram.com/lion_logic/" target="_blank">Instagram</a></li>
        </ul>
    </aside>
    <!-- end left-side -->
    <div class="scroll-down"><small><?=$language['translate']['common']['scroll_down']?></small><span></span></div>
    <!-- end scroll-down -->
    <div class="sound"> <span> <?=$language['translate']['common']['sound']?> </span>
        <div class="equalizer">
            <div class="holder"> <span></span> <span></span> <span></span> <span></span><span></span><span></span> </div>
            <!-- end holder -->
        </div>
        <!-- end equalizer -->
    </div>
    <!-- end sound -->
    <div class="swiper-slider">
        <div class="swiper-wrapper">
            <div class="swiper-slide" data-tilt data-tilt-max="5" data-tilt-speed="500" data-tilt-perspective="1500">
                <div class="slide-inner bg-image" data-background="/assets/images/slide01.jpg">
                    <div class="container">
                        <div class="tagline"><span>01</span>
                            <h6><?=$language['translate']['index']['hero']['first']['top']?></h6>
                        </div>
                        <!-- end tagline -->
                        <h1><?=$language['translate']['index']['hero']['first']['middle']?><br>
                            <span><?=$language['translate']['index']['hero']['first']['bottom']?></span></h1>
                        <div class="slide-btn"> <a href="#">
                                <div class="lines"> <span></span> <span></span> </div>
                                <!-- end lines -->
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     viewBox="0 0 104 104" enable-background="new 0 0 104 104" xml:space="preserve">
                <circle class="video-play-circle" fill="none" stroke="#fff" stroke-width="2" stroke-miterlimit="1" cx="52" cy="52" r="50"/>
              </svg>
                                <b><?=$language['translate']['common']['more']?></b> </a> </div>
                        <!-- end slide-btn -->
                    </div>
                    <!-- end container -->
                </div>
                <!-- end slide-inner -->
            </div>
            <!-- end swiper-slide -->

            <div class="swiper-slide" data-tilt data-tilt-max="5" data-tilt-speed="500" data-tilt-perspective="1500">
                <div class="slide-inner bg-image" data-background="/assets/images/slide03.jpg">
                    <div class="container">
                        <div class="tagline"><span>02</span>
                            <h6><?=$language['translate']['index']['hero']['second']['top']?></h6>
                        </div>
                        <!-- end tagline -->
                        <h1><?=$language['translate']['index']['hero']['second']['middle']?><br>
                            <span><?=$language['translate']['index']['hero']['second']['bottom']?></span></h1>
                        <div class="slide-btn"> <a href="#">
                                <div class="lines"> <span></span> <span></span> </div>
                                <!-- end lines -->
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     viewBox="0 0 104 104" enable-background="new 0 0 104 104" xml:space="preserve">
                <circle class="video-play-circle" fill="none" stroke="#fff" stroke-width="2" stroke-miterlimit="1" cx="52" cy="52" r="50"/>
              </svg>
                                <b><?=$language['translate']['common']['more']?></b> </a> </div>
                        <!-- end slide-btn -->
                    </div>
                    <!-- end container -->
                </div>
                <!-- end slide-inner -->
            </div>
            <!-- end swiper-slide -->

            <div class="swiper-slide" data-tilt data-tilt-max="5" data-tilt-speed="500" data-tilt-perspective="1500">
                <div class="slide-inner bg-image" data-background="/assets/images/slide02.jpg">
                    <div class="container">
                        <div class="tagline"><span>03</span>
                            <h6><?=$language['translate']['index']['hero']['third']['top']?></h6>
                        </div>
                        <!-- end tagline -->
                        <h1><?=$language['translate']['index']['hero']['third']['middle']?>
                        </h1>
                        <div class="slide-btn"> <a href="#">
                                <div class="lines"> <span></span> <span></span> </div>
                                <!-- end lines -->
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     viewBox="0 0 104 104" enable-background="new 0 0 104 104" xml:space="preserve">
                <circle class="video-play-circle" fill="none" stroke="#fff" stroke-width="2" stroke-miterlimit="1" cx="52" cy="52" r="50"/>
              </svg>
                                <b><?=$language['translate']['common']['more']?></b> </a> </div>
                        <!-- end slide-btn -->
                    </div>
                    <!-- end container -->
                </div>
                <!-- end slide-inner -->
            </div>
            <!-- end swiper-slide -->

        </div>
        <!-- end swiper-wrapper -->
        <div class="swiper-pagination"></div>
        <!-- end swiper-pagination -->
        <div class="swiper-fraction"></div>
        <!-- end swiper-fraction -->
    </div>
    <!-- end swiper-slider -->
</header>
<!-- end header -->
<style>
    a.knopka {
        color: #fff; /* цвет текста */
        text-decoration: none; /* убирать подчёркивание у ссылок */
        user-select: none; /* убирать выделение текста */
        background: rgb(212,75,56); /* фон кнопки */
        padding: 0.7em 1.5em;/* отступ от текста */
        outline: none; /* убирать контур в Mozilla */
    }
    a.knopka:hover { background: rgb(232,95,76); } /* при наведении курсора мышки */
    a.knopka:active { background: rgb(152,15,0); } /* при нажатии */
</style>
<section class="intro">
    <div class="container">
        <div class="row">
            <div class="col-12 wow fadeIn">
                <h6><?=$language['translate']['index']['warranty']['title']?></h6>
                <h2 data-text="Lion-Logic"><span><?=$language['translate']['index']['warranty']['subtitle']?></span><img src="/assets/images/emoji-rock.png" alt="создание и продвижение сайтов" class="rock"></h1></h2>
            </div>
            <!-- end col-12 -->
            <div class="col-lg-5 wow fadeIn">
                <h4><?=$language['translate']['index']['warranty']['col1']['text']?></h4><br><a href="<?=$language['translate']['index']['warranty']['col1']['button']['link']?>" class="knopka"><?=$language['translate']['index']['warranty']['col1']['button']['text']?></a>
            </div>
            <!-- end col-5 -->
            <div class="col-lg-7 wow fadeIn" data-wow-delay="0.10s">
                <br>
                <p>
                    <?=$language['translate']['index']['warranty']['col2']['text']?>
                </p>
                <a href="<?=$language['translate']['index']['warranty']['col2']['button']['link']?>" class="knopka"><?=$language['translate']['index']['warranty']['col2']['button']['text']?></a>
                <!-- end custom-link -->
            </div>
            <!-- end col-7 -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</section>
<!-- end intro -->
<section class="services-content-block">
    <div class="video-bg">
        <video src="/assets/videos/video.mp4" muted loop autoplay></video>
    </div>
    <!-- end video-bg -->
    <div class="container">
        <div class="row">
            <div class="col-12 wow fadeIn">
                <h6><?=$language['translate']['index']['approach']['title']?></h6>
                <h2><?=$language['translate']['index']['approach']['subtitle']?></h2>
            </div>
            <!-- end col-12 -->

            <?php foreach ($language['translate']['index']['approach']['items'] as $key => $value): ?>
                <div class="col-md-4 wow fadeIn" data-wow-delay="0s">
                    <div class="content-box">
                        <div class="left"><small><?=$key?></small><span></span></div>
                        <!-- end left -->
                        <div class="right">
                            <p><?=$value?></p>
                        </div>
                        <!-- end right -->
                    </div>
                    <!-- end content-box -->
                </div>
                <!-- end col-4 -->
            <?php endforeach; ?>

        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</section>
<!-- end services-content-block -->
<section class="works" id="works">
    <div class="container">
        <div class="row">
            <div class="col-12 wow fadeIn">
                <h6><?=$language['translate']['index']['features']['title']?></h6>
                <h2 data-text="Lion-Logic"><?=$language['translate']['index']['features']['subtitle']?></h2>
            </div>
            <!-- end col-12 -->

            <div class="col-12">
                <?php foreach ($language['translate']['index']['features']['items'] as $key => $value): ?>
                    <div class="project-box wow fadeIn" data-bg="<?=$value['data_bg']?>">
                        <figure> <a href="/assets/images/<?=$value['img']?>" data-fancybox><img src="/assets/images/<?=$value['img']?>" alt="<?=$value['title_color']?> <?=$value['title']?>"></a></figure>
                        <div class="content-box">
                            <div class="inner"> <small><?=$value['text']?></small>
                                <h3><span><?=$value['title_color']?></span><?=$value['title']?></h3>
                                <div class="custom-link"> <a href="<?=$value['button']['link']?>">
                                        <div class="lines"> <span></span> <span></span> </div>
                                        <!-- end lines -->
                                        <b><?=$value['button']['text']?></b></a> </div>
                                <!-- end custom-link -->
                            </div>
                            <!-- end inner -->
                        </div>
                        <!-- end content-box -->
                    </div>
                    <!-- end project-box -->
                <?php endforeach; ?>
            </div>
            <!-- end col-12 -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</section>
<!-- end works -->
<!--<section class="clients">
  <div class="container">
    <div class="row">
      <div class="col-lg-5 wow fadeIn">
        <h6>Клиенты</h6>
        <h2>Клиенты «Lion-Logic»</h2>
        <h4>Оставьте контакты, чтобы обсудить вашу задачу</h4>
        <div class="custom-link wow fadeIn"> <a href="#">
          <div class="lines"> <span></span> <span></span> </div>
          <b>BE OUR CLIENT</b></a> </div>
      </div>
      <div class="col-lg-7 wow fadeIn" data-wow-delay="0.10s">
        <ul>
          <li><img src="/assets/images/logo01.png" alt="Image"><small>ABSTER</small></li>
          <li><img src="/assets/images/logo02.png" alt="Image"><small>LOKOMOTIVE</small></li>
          <li><img src="/assets/images/logo03.png" alt="Image"><small>BIRDIEST</small></li>
          <li><img src="/assets/images/logo04.png" alt="Image"><small>PLOCSHA</small></li>
          <li><img src="/assets/images/logo05.png" alt="Image"><small>NEWKY</small></li>
          <li><img src="/assets/images/logo06.png" alt="Image"><small>HACHAPURY</small></li>
          <li><img src="/assets/images/logo02.png" alt="Image"><small>LOKOMOTIVE</small></li>
          <li><img src="/assets/images/logo05.png" alt="Image"><small>NEWKY</small></li>
        </ul>
      </div>
    </div>
  </div>
</section>-->
<section class="hello">
    <div class="container">
        <div class="row">
            <div class="col-12 wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                <h6><?=$language['translate']['index']['contacts']['title']?></h6>
                <h2 data-text="Lion-Logic"><?=$language['translate']['index']['contacts']['subtitle']?></h2>
            </div>
            <!-- end col-12 -->
            <div class="col-md-4 wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                <address>
                    <b><?=$language['translate']['common']['address']?></b>

                    <?php foreach ($language['translate']['contacts']['addresses'] as $key => $value): ?>
                        <p><?=$value?></p>
                    <?php endforeach; ?>

                </address>
            </div>
            <!-- end col-4 -->
            <div class="col-md-4 wow fadeIn" data-wow-delay="0.05s" style="visibility: visible; animation-delay: 0.05s; animation-name: fadeIn;">
                <address>
                    <b><?=$language['translate']['common']['phone']?></b>
                    <?php foreach ($language['translate']['contacts']['phones'] as $key => $value): ?>
                        <a href="tel:<?=$value?>">
                            <p><?=$value?></p>
                        </a>
                    <?php endforeach; ?>
                </address>
            </div>
            <!-- end col-4 -->
            <div class="col-md-4 wow fadeIn" data-wow-delay="0.10s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeIn;">
                <address>
                    <b>E-mail</b>
                    <?php foreach ($language['translate']['contacts']['emails'] as $key => $value): ?>
                        <a href="mailto:<?=$value?>">
                            <p><?=$value?></p>
                        </a>
                    <?php endforeach; ?>
                </address>
            </div>
            <!-- end col-4 -->
        </div>
        <!-- end row -->
        <div class="row align-items-center">
            <div class="col-lg-6 wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                <img src="/assets/images/map.png">
                <!-- end map -->
            </div>
            <!-- end col-6 -->
            <div class="col-lg-6 wow fadeIn" data-wow-delay="0.05s" style="visibility: visible; animation-delay: 0.05s; animation-name: fadeIn;">
                <form class="row inner" id="contact" name="contact" method="post" novalidate="novalidate">
                    <div class="form-group col-sm-6 col-12">
                        <label><span><?=$language['translate']['contact-form']['name']?></span></label>
                        <input type="text" name="name" id="name" required="">
                    </div>
                    <!-- end form-group -->
                    <div class="form-group col-sm-6 col-12">
                        <label><span><?=$language['translate']['contact-form']['phone']?></span></label>
                        <input type="text" name="surname" id="surname" required="">
                    </div>

                    <!-- end form-group -->
                    <div class="form-group col-12">
                        <label><span><?=$language['translate']['contact-form']['message']?></span></label>
                        <textarea name="message" id="message" required=""></textarea>
                    </div>
                    <!-- end form-group -->
                    <div class="form-group col-12">
                        <button id="submit" type="submit" name="submit"><?=$language['translate']['contact-form']['send']?></button> <div class="checkbox" style="padding-top: 25px;"><label style="font-size: 11px;"><input type="checkbox" value="1" aria-invalid="false" checked="checked"><?=$language['translate']['contact-form']['privacy']?></label></div>
                    </div>
                    <!-- end form-group -->
                </form>
                <!-- end form -->
                <div id="success" class="alert alert-success" role="alert"> Ваше сообщение было успешно отправлено! Мы свяжемся с вами, в ближайшее время. </div>
                <!-- end success -->
                <div id="error" class="alert alert-danger" role="alert"> Что-то пошло не так, попробуйте обновить и снова отправить заявку. </div>
                <!-- end error -->
            </div>
            <!-- end col-6 -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</section>
