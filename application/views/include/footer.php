<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!-- end clients -->
<footer class="footer">
    <div class="footer-quote wow fadeIn">
        <div class="container">
            <div class="row">
                <div class="col-lg-8"> <img src="/assets/images/logo.png" alt="создание и продвижение сайтов">
                    <h2></h2>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end footer-quote -->

    <!-- end footer-contact -->
    <div class="footer-bottom wow fadeIn">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <h5><?=$language['translate']['footer']['social-media']?></h5>
                    <ul>
                        <li><a href="https://vk.com/lion_logic" target="_blank">VK</a></li>
                        <li><a href="https://www.instagram.com/lion_logic/" target="_blank"></li>
                    </ul>
                </div>
                <!-- end col-8 -->
            </div><br><br>
            © 2008–2020 Lion-Logic.com. <br><?=$language['translate']['footer']['site-creation']?><br><br><a href=""><font color=ffffff><?=$language['translate']['footer']['privacy']?></font></a>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end footer-bottom -->
</footer>
<!-- end footer -->

<audio id="hamburger-hover" src="/assets/audio/link.mp3" preload="auto"></audio>

<!-- JS FILES -->
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/swiper.min.js"></script>
<script src="/assets/js/tilt.jquery.js"></script>
<script src="/assets/js/wow.min.js"></script>
<script src="/assets/js/odometer.min.js"></script>
<script src="/assets/js/jquery.typewriter.js"></script>
<script src="/assets/js/fancybox.min.js"></script>
<script src="/assets/js/app.js"></script>
<script src="/assets/js/scripts.js"></script>
</body>
</html>