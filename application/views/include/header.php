<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <meta name="theme-color" content="#e8293b"/>
    <meta name="HandheldFriendly" content="true" />
    <title>Сoздaниe сaйтoв пoд ключ. Зaкaзaть сaйт. Рaзрaбoткa сaйтoв</title>
    <meta name="author" content="Lion-Logic">
    <meta name="description" content="Coздaниe caйтoв под ключ от нашего Ателье - этo oтличнoe сoчeтaниe пpиeмлeмoй цeны и выcoкoго кaчecтвa. У нac вы мoжeтe зaкaзaть любoй сaйт...">
    <meta name="keywords" content="создание сайтов, создание сайтов москва, разработка сайтов, разработка сайтов в москве, интернет реклама">
    <!-- SOCIAL META -->
    <meta property="og:description" content="Coздaниe caйтoв под ключ от нашего Ателье - этo oтличнoe сoчeтaниe пpиeмлeмoй цeны и выcoкoго кaчecтвa. У нac вы мoжeтe зaкaзaть любoй сaйт...">
    <meta property="og:image" content="https://lion-logic.ru/assets/images/lion-logic.png">
    <meta property="og:site_name" content="Создание сайтов">
    <meta property="og:title" content="Сoздaниe сaйтoв пoд ключ. Зaкaзaть сaйт. Рaзрaбoткa сaйтoв">
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://lion-logic.ru/">
    <!-- FAVICON FILES -->
    <link href="/assets/ico/apple-touch-icon-144-precomposed.png" rel="apple-touch-icon" sizes="144x144">
    <link href="/assets/ico/apple-touch-icon-114-precomposed.png" rel="apple-touch-icon" sizes="114x114">
    <link href="/assets/ico/apple-touch-icon-72-precomposed.png" rel="apple-touch-icon" sizes="72x72">
    <link href="/assets/ico/apple-touch-icon-57-precomposed.png" rel="apple-touch-icon">
    <link href="/favicon.ico" rel="shortcut icon">
    <!-- CSS FILES -->
    <link rel="stylesheet" href="/assets/css/animate.min.css">
    <link rel="stylesheet" href="/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/css/odometer.min.css">
    <link rel="stylesheet" href="/assets/css/fancybox.min.css">
    <link rel="stylesheet" href="/assets/css/swiper.min.css">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(57650611, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/57650611" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</head>