<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<body>
<div class="preloader">
    <div class="layer"></div>
    <div class="layer"></div>
    <div class="layer"></div>
    <div class="layer"></div>
    <div class="inner" data-tilt data-tilt-perspective="1000">
        <figure class="fadeInUp animated"> <img src="/assets/images/preloader.gif"></figure>
        <span class="typewriter" id="typewriter"></span> </div>
    <!-- end inner -->
</div>
<!-- end preloader -->
<div class="transition-overlay">
    <div class="layer"></div>
    <div class="layer"></div>
    <div class="layer"></div>
    <div class="layer"></div>
</div>
<!-- end transition-overlay -->
<div class="navigation-menu">
    <div class="bg-layers"> <span></span> <span></span> <span></span> <span></span> </div>
    <!-- end bg-layers -->
    <div class="inner" data-tilt data-tilt-perspective="2000">
        <div class="menu">
            <ul>
                <li><a href="/"><?=$language['translate']['common']['menu']['main']?></a></li>
                <li><a href="/portfolio"><?=$language['translate']['common']['menu']['portfolio']?></a></li>
                <!--<li><a href="news.html">Новости</a></li>-->
                <li><a href="/#contact"><?=$language['translate']['common']['menu']['contacts']?></a></li>
            </ul>
        </div>
        <!-- end menu -->
        <blockquote><?=$language['translate']['common']['menu']['quote']?></blockquote>
    </div>
    <!-- end inner -->
</div>
<!-- end navigation-menu -->
<nav class="navbar">
    <div class="left"> <a href="/portfolio"><?=$language['translate']['common']['menu']['portfolio']?></a> </div>
    <!-- end left -->
    <div class="logo"> <a href="/"><img src="/assets/images/logo.png" alt="создание и продвижение сайтов"></a> </div>
    <!-- end logo -->
    <div class="right">
        <ul class="language">
            <li><a href="/Main/changeLoc/en">EN</a></li>
            <li><a href="/Main/changeLoc/ru">RU</a></li>
        </ul>
        <div class="hamburger-menu"><b><?=$language['translate']['common']['menu']['menu']?></b>
            <div class="hamburger" id="hamburger-menu"> <span></span> <span></span> <span></span> </div>
        </div>
        <!-- end hamburger-menu -->
    </div>
    <!-- end right -->
</nav>
<!-- end navbar -->