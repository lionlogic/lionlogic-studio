<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<header class="page-header">
    <div class="video-bg">
        <video src="/assets/videos/video2.mp4" muted loop autoplay></video>
    </div>
    <!-- end video-bg -->
    <div class="container">
        <h1><?=$language['translate']['portfolio']['hero']['title']?></h1>
        <p><?=$language['translate']['portfolio']['hero']['subtitle']?></p>
    </div>
    <!-- end container -->
    <aside class="left-side">
        <ul>
            <li><a href="#">VK</a></li>
            <li><a href="#">Instagram</a></li>
            <li><a href="#">Facebook</a></li>
        </ul>
    </aside>
    <!-- end left-side -->
    <div class="scroll-down"><small><?=$language['translate']['common']['scroll_down']?></small><span></span></div>
    <!-- end scroll-down -->
    <div class="sound"> <span> <?=$language['translate']['common']['sound']?> </span>
        <div class="equalizer">
            <div class="holder"> <span></span> <span></span> <span></span> <span></span><span></span><span></span> </div>
            <!-- end holder -->
        </div>
        <!-- end equalizer -->
    </div>
    <!-- end sound -->
</header>
<!-- end header -->
<section class="works">
    <div class="container">
        <div class="row">
            <div class="col-12 wow fadeIn">
                <h6><?=$language['translate']['portfolio']['works']['title']?></h6>
                <h2 data-text="Lion-Logic"><?=$language['translate']['portfolio']['works']['subtitle']?></h2>
            </div>
            <!-- end col-12 -->
            <div class="col-12">
                <div class="project-box wow fadeIn" data-bg="#faf8ed">
                    <figure> <a href="/assets/images/portfolio/pr1.png" data-fancybox><img src="/assets/images/portfolio/pr1.png" alt="веб студия москва"></a></figure>
                    <div class="content-box">
                        <div class="inner"> <small>Сегодня многие люди обращают внимание не только на стоимость мебели, но и на ее непревзойдённые эстетические качества.</small>
                            <h3><span>Итальянская</span>мягкая мебель</h3>
                           <div class="custom-link"> <a href="http://www.cuborosso.ru/" rel="nofollow">
                                        <div class="lines"> <span></span> <span></span> </div>
                                        <!-- end lines -->
                                        <b>Подробней</b></a></div>
                            <!-- end custom-link -->
                        </div>
                        <!-- end inner -->
                    </div>
                    <!-- end content-box -->
                </div>
                <!-- end project-box -->
                <div class="project-box wow fadeIn" data-bg="#ece6f4">
                    <figure> <a href="/assets/images/portfolio/pr2.png" data-fancybox><img src="/assets/images/portfolio/pr2.png" alt="создание сайтов"></a></figure>
                    <div class="content-box">
                        <div class="inner"> <small>Завод АГМА производит строительную сетку. Металлическая сетка партиями любого объема, по ценам производителя</small>
                            <h3><span>Производство</span>строительных сеток</h3>
                           <div class="custom-link"> <a href="http://zavodagma.ru/" rel="nofollow">
                                        <div class="lines"> <span></span> <span></span> </div>
                                        <!-- end lines -->
                                        <b>Подробней</b></a> </div>
                            <!-- end custom-link -->
                        </div>
                        <!-- end inner -->
                    </div>
                    <!-- end content-box -->
                </div>
                <!-- end project-box -->
                <div class="project-box wow fadeIn" data-bg="#ebf8f3">
                    <figure> <a href="/assets/images/portfolio/pr3.png" data-fancybox><img src="/assets/images/portfolio/pr3.png" alt="создание сайтов в москве"></a></figure>
                    <div class="content-box">
                        <div class="inner"> <small>Одни сказки читают, а другие в них живут</small>
                            <h3><span>Шоу мыльных пузырей</span>Инны Левинтан</h3>
                           <div class="custom-link"> <a href="https://karambashow.ru/" rel="nofollow">
                                        <div class="lines"> <span></span> <span></span> </div>
                                        <!-- end lines -->
                                        <b>Подробней</b></a> </div>
                            <!-- end custom-link -->
                        </div>
                        <!-- end inner -->
                    </div>
                    <!-- end content-box -->
                </div>
                <!-- end project-box -->
                <div class="project-box wow fadeIn" data-bg="#f4eedf">
                    <figure> <a href="/assets/images/portfolio/pr4.png" data-fancybox><img src="/assets/images/portfolio/pr4.png" alt="заказать сайт москва"></a></figure>
                    <div class="content-box">
                        <div class="inner"> <small>Деньги под залог недвижимости, личного автомобиля, ипотека</small>
                            <h3><span>Залогово</span>ипотечный центр</h3>
                           <div class="custom-link"> <a href="https://finkonsalt63.ru/" rel="nofollow">
                                        <div class="lines"> <span></span> <span></span> </div>
                                        <!-- end lines -->
                                        <b>Подробней</b></a> </div>
                            <!-- end custom-link -->
                        </div>
                        <!-- end inner -->
                    </div>
                    <!-- end content-box -->
                </div>
                <!-- end project-box -->
                <div class="project-box wow fadeIn" data-bg="#e5f2f7">
                    <figure> <a href="/assets/images/portfolio/pr5.png" data-fancybox><img src="/assets/images/portfolio/pr5.png" alt="создать сайт под ключ"></a></figure>
                    <div class="content-box">
                        <div class="inner"> <small>Кондитерская студия и мастерская Юлии Николенко</small>
                            <h3><span>Авторские торты</span>Юлии Николенко</h3>
                           <div class="custom-link"> <a href="https://julianikolenko.ru/" rel="nofollow">
                                        <div class="lines"> <span></span> <span></span> </div>
                                        <!-- end lines -->
                                        <b>Подробней</b></a> </div>
                            <!-- end custom-link -->
                        </div>
                        <!-- end inner -->
                    </div>
                    <!-- end content-box -->
                </div>
                <!-- end project-box -->
                <div class="project-box wow fadeIn" data-bg="#f5efe8">
                    <figure> <a href="/assets/images/portfolio/pr6.png" data-fancybox><img src="/assets/images/portfolio/pr6.png" alt="продвижение сайтов"></a></figure>
                    <div class="content-box">
                        <div class="inner"> <small>Сеть кафе & Доставка</small>
                            <h3><span>Империя Пиццы</span>доставка пиццы</h3>
                           <div class="custom-link"> <a href="https://ipizza.ru/" rel="nofollow">
                                        <div class="lines"> <span></span> <span></span> </div>
                                        <!-- end lines -->
                                        <b>Подробней</b></a> </div>
                            <!-- end custom-link -->
                        </div>
                        <!-- end inner -->
                    </div>
                    <!-- end content-box -->
                </div>
                <!-- end project-box -->
                <div class="project-box wow fadeIn" data-bg="#dbe3f1">
                    <figure> <a href="/assets/images/portfolio/pr7.png" data-fancybox><img src="/assets/images/portfolio/pr7.png" alt="разработка сайтов"></a></figure>
                    <div class="content-box">
                        <div class="inner"> <small>Один из ведущих производителей в России</small>
                            <h3><span>Кубанские</span>раки оптом</h3>
                           <div class="custom-link"> <a href="http://kubani-raki.ru/" rel="nofollow">
                                        <div class="lines"> <span></span> <span></span> </div>
                                        <!-- end lines -->
                                        <b>Подробней</b></a> </div>
                            <!-- end custom-link -->
                        </div>
                        <!-- end inner -->
                    </div>
                    <!-- end content-box -->
                </div>
                <!-- end project-box -->
                <div class="project-box wow fadeIn" data-bg="#e8d7f1">
                    <figure> <a href="/assets/images/portfolio/pr8.png" data-fancybox><img src="/assets/images/portfolio/pr8.png" alt="создание сайтов под ключ"></a></figure>
                    <div class="content-box">
                        <div class="inner"> <small>Специализированная студия йоги Айенгара в Одинцово</small>
                            <h3><span>Студия йоги</span>в Одинцово</h3>
                           <div class="custom-link"> <a href="https://odinyoga.ru/" rel="nofollow">
                                        <div class="lines"> <span></span> <span></span> </div>
                                        <!-- end lines -->
                                        <b>Подробней</b></a> </div>
                            <!-- end custom-link -->
                        </div>
                        <!-- end inner -->
                    </div>
                    <!-- end content-box -->
                </div>
                <!-- end project-box -->
                <div class="project-box wow fadeIn" data-bg="#f1dad6">
                    <figure> <a href="/assets/images/portfolio/pr9.png" data-fancybox><img src="/assets/images/portfolio/pr9.png" alt="сайт под ключ"></a></figure>
                    <div class="content-box">
                        <div class="inner"> <small>Участки на большой воде по доступным ценам</small>
                            <h3><span>Коттеджный поселок</span> «Три реки»</h3>
                           <div class="custom-link"> <a href="https://3-reki.ru/" rel="nofollow">
                                        <div class="lines"> <span></span> <span></span> </div>
                                        <!-- end lines -->
                                        <b>Подробней</b></a> </div>
                            <!-- end custom-link -->
                        </div>
                        <!-- end inner -->
                    </div>
                    <!-- end content-box -->
                </div>
                <!-- end project-box -->
                <div class="project-box wow fadeIn" data-bg="#f1dad6">
                    <figure> <a href="/assets/images/portfolio/pr10.png" data-fancybox><img src="/assets/images/portfolio/pr10.png" alt="разработка сайтов"></a></figure>
                    <div class="content-box">
                        <div class="inner"> <small>Газовые колонки и газовые котлы</small>
                            <h3><span>ТЕРМО</span>СИСТЕМЫ</h3>
                           <div class="custom-link"> <a href="https://thermo-systems.ru/" rel="nofollow">
                                        <div class="lines"> <span></span> <span></span> </div>
                                        <!-- end lines -->
                                        <b>Подробней</b></a> </div>
                            <!-- end custom-link -->
                        </div>
                        <!-- end inner -->
                    </div>
                    <!-- end content-box -->
                </div>
            </div>
            <!-- end col-12 -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</section>
<!-- end works -->